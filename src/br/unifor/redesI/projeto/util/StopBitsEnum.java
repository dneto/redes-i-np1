/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifor.redesI.projeto.util;

import gnu.io.SerialPort;

/**
 *
 * @author Demétrio
 */
public enum StopBitsEnum {

    STOP_1(SerialPort.STOPBITS_1, "1"),
    STOP_1_5(SerialPort.STOPBITS_1_5, "1.5"),
    STOP_2(SerialPort.STOPBITS_2, "2");
    int value;
    String displayValue;

    private StopBitsEnum(int value, String displayValue) {
        this.value = value;
        this.displayValue = displayValue;
    }

    @Override
    public String toString() {
        return displayValue;
    }

    public static StopBitsEnum getByDisplayValue(String displayValue) {
        for (StopBitsEnum e : StopBitsEnum.values()) {
            if (e.displayValue.equals(displayValue)) {
                return e;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }
}
