package br.unifor.redesI.projeto.util;

import br.unifor.redesI.projeto.util.exception.EmptyQueueException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Demétrio
 */
public class Queue<T> {

    private List<T> list;
    private final Object lockObject;

    public Queue() {
        this.lockObject = new Object();
        this.list = new ArrayList<>();
    }

    public void enqueue(T obj) {
        synchronized (lockObject) {
            list.add(obj);
        }
    }

    public T dequeue() {
        synchronized (lockObject) {
            if (list.size() > 0) {
                return list.remove(0);
            }
        }

        throw new RuntimeException(new EmptyQueueException());
    }

    public int size() {
        return list.size();
    }

    public synchronized boolean isEmpty() {
        return list.isEmpty();
    }

    public synchronized boolean isNotEmpty() {
        return !list.isEmpty();
    }
}
