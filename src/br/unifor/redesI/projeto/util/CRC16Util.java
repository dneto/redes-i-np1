/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifor.redesI.projeto.util;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import sun.misc.CRC16;

/**
 *
 * @author Demétrio
 */
public class CRC16Util {

    List<Byte> crcList = new ArrayList<>();
    
    public void addByte(byte b){
        crcList.add(b);
    }
    
    public byte[] CRC16() {

        CRC16 crc = new CRC16();

        for (byte b : crcList) {
            crc.update(b);
        }

        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.putShort((short) crc.value);

        return bb.array();
    }
}
