/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifor.redesI.projeto.util;

import gnu.io.SerialPort;

/**
 *
 * @author Demétrio
 */
public enum ParityEnum {
    NONE(SerialPort.PARITY_NONE,"None"), 
    EVEN(SerialPort.PARITY_EVEN,"Even"), 
    MARK(SerialPort.PARITY_MARK,"Mark"), 
    SPACE(SerialPort.PARITY_SPACE,"Space"), 
    ODD(SerialPort.PARITY_ODD, "Odd");
    
    int value;
    String displayValue;
    
    private ParityEnum(int value, String displayValue){
        this.value = value;
        this.displayValue = displayValue;
    }

    @Override
    public String toString() {
        return displayValue;
    }
    
    public static ParityEnum getByDisplayValue(String displayValue) {
        for (ParityEnum e : ParityEnum.values()) {
            if (e.displayValue.equals(displayValue)) {
                return e;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }
    
    
}
