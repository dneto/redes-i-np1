/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifor.redesI.projeto.util;

import gnu.io.SerialPort;

/**
 *
 * @author Demétrio
 */
public enum DataBitsEnum {

    DATABITS_5(SerialPort.DATABITS_5, "5"), 
    DATABITS_6(SerialPort.DATABITS_6, "6"), 
    DATABITS_7(SerialPort.DATABITS_7, "7"), 
    DATABITS_8(SerialPort.DATABITS_8, "8");
    
    int constant;
    String displayValue;

    private DataBitsEnum(int constant, String displayValue) {
        this.constant = constant;
        this.displayValue = displayValue;
    }

    @Override
    public String toString() {
        return displayValue;
    }
    
    public static DataBitsEnum getByDisplayValue(String displayValue){
        for(DataBitsEnum e: DataBitsEnum.values()){
            if(e.displayValue.equals(displayValue))
                return e;
        }
        return null;
    }
    
    public int getValue(){
        return constant;
    }
}
