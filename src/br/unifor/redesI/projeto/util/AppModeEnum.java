/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifor.redesI.projeto.util;

/**
 *
 * @author Demétrio
 */
public enum AppModeEnum {
    SEND(0,"Send"), RECEIVE(1,"Receive");
    
    private int value;
    private String displayValue;
    
    private AppModeEnum(int value, String displayValue){
        this.value = value;
        this.displayValue = displayValue;
    }

    @Override
    public String toString() {
        return displayValue; 
    }
    
}
