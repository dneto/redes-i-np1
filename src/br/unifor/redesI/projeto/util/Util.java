/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifor.redesI.projeto.util;

/**
 *
 * @author Demétrio
 */
public class Util {
    public static void printByteArrayHexa(byte[] array){
        StringBuffer bf = new StringBuffer();
        for(byte b: array){
            bf.append(String.format("%X", b));
        }
        
        System.out.println(bf.toString());
    }
}
