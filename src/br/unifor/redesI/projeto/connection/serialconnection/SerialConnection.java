package br.unifor.redesI.projeto.connection.serialconnection;

import br.unifor.redesI.projeto.connection.Connection;
import br.unifor.redesI.projeto.connection.ConnectionListener;
import br.unifor.redesI.projeto.protocol.wrbprotocol.WhiteRedBlueProtocol;
import br.unifor.redesI.projeto.util.DataBitsEnum;
import br.unifor.redesI.projeto.util.ParityEnum;
import br.unifor.redesI.projeto.util.StopBitsEnum;
import br.unifor.redesI.projeto.util.exception.TimeoutException;
import gnu.io.CommPortIdentifier;
import gnu.io.CommPort;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class representing a Serial Connection.
 *
 * @author Alexandre Pinheiro Demétrio Menezes Neto Ingrid Aguiar
 */
public class SerialConnection implements Connection<byte[]> {

    private int numOfTries;
    private int timeout;
    private String port;
    private String applicationName;
    private SerialPort serialPort;
    private InputStream inputStream;
    private OutputStream outputStream;
    private List<ConnectionListener<byte[]>> serialReaders;

    public SerialConnection(String applicationName, String portName, int bitRate, DataBitsEnum dataBits,
            ParityEnum parity, StopBitsEnum stopBits, int numOfTries, int timeout) {
        this.port = portName;
        this.applicationName = applicationName;
        this.serialReaders = new ArrayList<>();

        this.numOfTries = numOfTries;
        this.timeout = timeout;
    }

    /**
     * Initializes a new serial connection.
     */
    public void connect() {
        try {
            CommPortIdentifier commPortIdentifier = CommPortIdentifier.getPortIdentifier(this.port);
            if (commPortIdentifier.isCurrentlyOwned()) {
                //
            } else {
                CommPort commPort = commPortIdentifier.open(applicationName, 2000);
                this.serialPort = (SerialPort) commPort;
                this.setSerialPortParameters();
                this.inputStream = serialPort.getInputStream();
                this.outputStream = serialPort.getOutputStream();
            }
        } catch (NoSuchPortException | PortInUseException | IOException ex) {
            Logger.getLogger(SerialConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Sent a message over serial connection.
     *
     * @param message Message to be sent over serial connection.
     */
    @Override
    public byte write(byte[] message, boolean waitResponse) throws TimeoutException {
        try {

            outputStream.write(message);
            outputStream.flush();



            if (waitResponse) {
                byte[] buffer;
                int len = -1;
                long timeOutInit = System.currentTimeMillis();

                while ((len = inputStream.available()) > -1) {
                    if (len == 3) {
                        buffer = new byte[len];

                        for (int i = 0; i < len; i++) {
                            buffer[i] = (byte) inputStream.read();
                        }

                        byte byteParaRetornar = WhiteRedBlueProtocol.NACK_BYTE;
                        for (int i = 0; i < buffer.length; i++) {
                            byte b = buffer[i];
                            switch (b) { // Se B for ACK ou NACK...
                                case WhiteRedBlueProtocol.SYN_BYTE:
                                    System.err.printf("SYN");
                                    break;
                                case WhiteRedBlueProtocol.PAD_BYTE:
                                    System.err.println("PAD");
                                    break;
                                case WhiteRedBlueProtocol.ACK_BYTE:
                                case WhiteRedBlueProtocol.NACK_BYTE:
                                    System.err.printf(" %X ", b);
                                    byteParaRetornar = b;
                                    break;
                                default:
                                    System.err.println("BYTE INESPERADO!!!!!!! " + b);
                            }
                        }
                        return byteParaRetornar;
                    } else {
                        if (System.currentTimeMillis() - timeOutInit > timeout) {
                            throw new TimeoutException();
                        }
                    }
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(SerialConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     * Start to listen messages sent over serial connection.
     */
    @Override
    public void startRead() {
        Thread read = new Thread(new SerialReaderAction(this), "Serious Chat Read Thread");

        read.setDaemon(
                true);
        read.start();
    }

    /**
     * Close the connection and respective streams.
     */
    public void close() {
        try {
            if (inputStream != null) {
                inputStream.close();
            }

            if (outputStream != null) {
                outputStream.close();
            }

            if (serialPort != null) {
                serialPort.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(SerialConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Add a serialReader to be used
     *
     * @param serialReader
     */
    public void addSerialReader(SerialReader serialReader) {
        serialReaders.add(serialReader);
    }

    private void setSerialPortParameters() {
        int bps = 57600;
        try {
            this.serialPort.setSerialPortParams(bps, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException ex) {
            Logger.getLogger(SerialConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void addConnectionListener(ConnectionListener<byte[]> reader) {
        serialReaders.add(reader);
    }

    @Override
    public int getTimeout() {
        return timeout;
    }

    @Override
    public int getTriesNum() {
        return numOfTries;
    }

    /**
     * Runnable class to handle received messages over serial connection
     */
    private static class SerialReaderAction implements Runnable {

        SerialConnection conn;

        public SerialReaderAction(SerialConnection conn) {
            this.conn = conn;
        }

        @Override
        public void run() {
            try {
                byte[] buffer;
                int len = -1;
                while ((len = conn.inputStream.available()) > -1) {
                    if (len > 0) {
                        buffer = new byte[len];
                        for (int i = 0; i < len; i++) {
                            buffer[i] = (byte) conn.inputStream.read();
                        }


                        System.out.print("Mensagem Recebida:");
                        br.unifor.redesI.projeto.util.Util.printByteArrayHexa(buffer);
                        for (ConnectionListener<byte[]> reader : conn.serialReaders) {
                            reader.readEvent(buffer);
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(SerialConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
