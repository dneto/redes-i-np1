package br.unifor.redesI.projeto.connection.serialconnection;

import br.unifor.redesI.projeto.connection.ConnectionListener;

/**
 * Interface that represents a event triggered when a new message is received
 * over serial connection.
 *
 * @author Alexandre Pinheiro Demétrio Menezes Neto Ingrid Aguiar
 */
public interface SerialReader extends ConnectionListener {
}
