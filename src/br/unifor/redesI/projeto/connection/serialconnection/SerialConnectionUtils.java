package br.unifor.redesI.projeto.connection.serialconnection;

import gnu.io.CommPortIdentifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Class with utilitary functions for serial connection.
 *
 * @author Alexandre Pinheiro Demétrio Menezes Neto Ingrid Aguiar
 */
public class SerialConnectionUtils {

    /**
     * List all serial ports.
     *
     * @return List of all serial ports
     */
    public static List<CommPortIdentifier> listPorts() {
        Enumeration<CommPortIdentifier> portEnumeration = CommPortIdentifier.getPortIdentifiers();
        List<CommPortIdentifier> portList = new ArrayList<>();
        while (portEnumeration.hasMoreElements()) {
            portList.add(portEnumeration.nextElement());
        }

        return portList;
    }

    /**
     * List all available serial ports.
     *
     * @return list of serial ports name.
     */
    public static List<String> listAvailablePorts() {
        Enumeration<CommPortIdentifier> portEnumeration = CommPortIdentifier.getPortIdentifiers();
        List<String> portList = new ArrayList<>();
        CommPortIdentifier cpi;

        while (portEnumeration.hasMoreElements()) {
            cpi = portEnumeration.nextElement();
            if (!cpi.isCurrentlyOwned()) {
                portList.add(cpi.getName());
            }
        }

        return portList;
    }
}
