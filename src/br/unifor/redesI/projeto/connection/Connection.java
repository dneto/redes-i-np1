package br.unifor.redesI.projeto.connection;

import br.unifor.redesI.projeto.util.exception.TimeoutException;

/**
 *
 * @author Demétrio
 */
public interface Connection<SEND_TYPE> {

    void connect();

    byte write(SEND_TYPE message, boolean waitResponse) throws TimeoutException;

    void startRead();

    void addConnectionListener(ConnectionListener<SEND_TYPE> reader);
    
    int getTimeout();
    
    int getTriesNum();
}
