package br.unifor.redesI.projeto.connection;

/**
 *
 * @author Demétrio
 */
public interface ConnectionListener<RECEIVE_TYPE> {

    void readEvent(RECEIVE_TYPE msg);
}
