package br.unifor.redesI.projeto.protocol.wrbprotocol;

import br.unifor.redesI.projeto.protocol.Protocol;

/**
 *
 * @author Demétrio
 */
public class WhiteRedBlueProtocol implements Protocol {

    public static final byte SYN_BYTE = (byte) 0xFB;
    public static final byte PAD_BYTE = (byte) 0xFF;
    public static final byte ACK_BYTE = (byte) 0xF8;
    public static final byte NACK_BYTE = (byte) 0xF9;
    
    public static int ID_FRAME_SIZE = 1;
    public static int SOURCE_SIZE = 1;
    public static int DESTINATION_SIZE = 1;
    public static int PAYLOAD_LENGTH_SIZE = 2;
    public static int CHECKSUM_SIZE = 2;
}
