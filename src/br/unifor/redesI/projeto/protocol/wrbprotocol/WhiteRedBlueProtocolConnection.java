package br.unifor.redesI.projeto.protocol.wrbprotocol;

import br.unifor.redesI.projeto.connection.Connection;
import br.unifor.redesI.projeto.connection.ConnectionListener;
import br.unifor.redesI.projeto.protocol.ProtocolConnection;
import br.unifor.redesI.projeto.util.CRC16Util;
import br.unifor.redesI.projeto.util.Queue;
import br.unifor.redesI.projeto.util.exception.NackException;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Demétrio
 */
public class WhiteRedBlueProtocolConnection implements ProtocolConnection {

    WhiteRedBlueProtocolWriter writer;
    Connection connection;
    private short payloadLength;
    private static byte idFrame = 0;
    private int secTimeout = 3000;
    private int sendTries = 3;
    private List<ConnectionListener<String>> connectionListeners = new ArrayList<>();
    private Queue<byte[]> receivedMsgs = new Queue<>();
    private ConnectionListener<byte[]> clistener = new ConnectionListener<byte[]>() {
        @Override
        public void readEvent(byte[] msg) {
            receivedMsgs.enqueue(msg);
        }
    };

    public WhiteRedBlueProtocolConnection(Connection connection, short payloadLength) {
        this.payloadLength = payloadLength;
        this.connection = connection;
        this.connection.addConnectionListener(clistener);
        this.writer = new WhiteRedBlueProtocolWriter(this);

    }

    @Override
    public void connect() {
        this.connection.connect();
    }

    @Override
    public byte write(String message, boolean waitResponse) {
        writer.write(message, this.payloadLength);
        return 0;
    }

    @Override
    public void startRead() {
        this.connection.startRead();
        ReadThread t = new ReadThread(this);
        t.start();
    }

    @Override
    public void sendACK() {
        writer.writeACKNACK(WhiteRedBlueProtocol.ACK_BYTE);
    }

    @Override
    public void sendNACK() {
        writer.writeACKNACK(WhiteRedBlueProtocol.NACK_BYTE);
    }

    @Override
    public void addConnectionListener(ConnectionListener reader) {
        connectionListeners.add(reader);
    }

    public int getPayloadLength() {
        return payloadLength;
    }

    public void setPayloadLength(short payloadLength) {
        this.payloadLength = payloadLength;
    }

    public static synchronized byte getIdFrame() {
        return ++idFrame;
    }

    public int getSecTimeout() {
        return secTimeout;
    }

    public void setSecTimeout(int secTimeout) {
        this.secTimeout = secTimeout;
    }

    public int getSendTries() {
        return sendTries;
    }

    public void setSendTries(int sendTries) {
        this.sendTries = sendTries;
    }

    protected Connection getConnection() {
        return connection;
    }

    @Override
    public int getTimeout() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getTriesNum() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String parse(byte[] byteArray) throws NackException {
        if (byteArray.length > 3) {
            byte[] msgRecebida = byteArray;
            int lenFrame = msgRecebida.length;
            CRC16Util crc16 = new CRC16Util();

            if (byteArray[0] == WhiteRedBlueProtocol.SYN_BYTE && byteArray[lenFrame - 1] == WhiteRedBlueProtocol.PAD_BYTE) {
                // Armazena dados para calculo do CRC16

                for (int i = 0; i < lenFrame - 3; i++) {
                    crc16.addByte(msgRecebida[i]);
                }

                // Armazena o byte PAD
                crc16.addByte(msgRecebida[lenFrame - 1]);

                // Calcula o CRC16
                byte[] crcCalculado = crc16.CRC16();

                // Testa o CRC16
                if (msgRecebida[lenFrame - 3] == crcCalculado[0]
                        && msgRecebida[lenFrame - 2] == crcCalculado[1]) {
                    // CRC16 invalido
                    this.sendACK();
                } else {
                    this.sendNACK();
                    throw new NackException();
                }

                byte[] dados = Arrays.copyOfRange(msgRecebida, 6, lenFrame - 3);
                String dadosMsg = new String(dados);

                // Verifica se último frame (sem dados)

                return dadosMsg;
            } else {
                System.err.println("FRAME MAL-FORMADO!!!");
            }
        }
        return "";
    }

    public static class ReadThread extends Thread {

        WhiteRedBlueProtocolConnection conn;

        public ReadThread(WhiteRedBlueProtocolConnection conn) {
            this.conn = conn;
        }

        @Override
        public void run() {
            StringBuffer strBuffer = new StringBuffer();
            List<Byte> byteList = new ArrayList<>();
            while (true) {
                if (conn.receivedMsgs.isNotEmpty()) {
                    byte[] receivedData = conn.receivedMsgs.dequeue();
                    for (byte b : receivedData) {
                        switch (b) {
                            case WhiteRedBlueProtocol.SYN_BYTE:
                                byteList = new ArrayList<>();
                                byteList.add(b);
                                break;

                            case WhiteRedBlueProtocol.PAD_BYTE:
                                //Pacote completo.
                                byteList.add(b);
                                byte[] barray = new byte[byteList.size()];

                                for (int i = 0; i < byteList.size(); i++) {
                                    System.err.printf("%X", byteList.get(i));
                                    barray[i] = byteList.get(i);
                                }
                                System.err.println("");

                                try {
                                    String msg = conn.parse(barray);
                                    System.out.println(msg);
                                    strBuffer.append(msg);

                                    if (msg.isEmpty()) {
                                        strBuffer.append("\n");
                                        String finalMsg = strBuffer.toString();
                                        System.out.println(finalMsg);
                                        for (ConnectionListener<String> cl : conn.connectionListeners) {
                                            cl.readEvent(finalMsg);
                                        }
                                        strBuffer = new StringBuffer(); // Criando StringBuffer para próxima mensagem.
                                    }

                                } catch (NackException ex) {
                                }

                                break;

                            default: // Demais bytes
                                byteList.add(b);
                                break;
                        }
                    }
                } else {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(WhiteRedBlueProtocolConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}
