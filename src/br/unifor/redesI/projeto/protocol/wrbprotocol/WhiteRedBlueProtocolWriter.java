package br.unifor.redesI.projeto.protocol.wrbprotocol;

import br.unifor.redesI.projeto.connection.Connection;
import br.unifor.redesI.projeto.protocol.ProtocolWriter;
import br.unifor.redesI.projeto.util.CRC16Util;
import br.unifor.redesI.projeto.util.Util;
import br.unifor.redesI.projeto.util.exception.TimeoutException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Demétrio
 */
public class WhiteRedBlueProtocolWriter implements ProtocolWriter {

    Connection<byte[]> connection;

    public WhiteRedBlueProtocolWriter(WhiteRedBlueProtocolConnection connection) {
        this.connection = connection.getConnection();
    }

    @Override
    public List<byte[]> parse(String msg, short payloadLength) {

        byte[] tmpMsgByte = msg.getBytes();

        List<byte[]> byteMsgList = new ArrayList<>();

        List<byte[]> byteMsgTmpList = split(tmpMsgByte, payloadLength);

        byte[] byteMsg;
        int index;

        for (byte[] msgPart : byteMsgTmpList) {
            CRC16Util crc16 = new CRC16Util();
            index = 0; //Reinicia o indice.

            byteMsg = new byte[9 + (msgPart.length)]; //Zera o Frame

            byteMsg[index] = WhiteRedBlueProtocol.SYN_BYTE;
            crc16.addByte(byteMsg[index]);
            index = index + 1;

            byteMsg[index] = WhiteRedBlueProtocolConnection.getIdFrame();
            crc16.addByte(byteMsg[index]);
            index = index + 1;

            byteMsg[index] = 1; //byteMsg[index++] = <ORIGEM> 
            crc16.addByte(byteMsg[index]);
            index = index + 1;

            byteMsg[index] = 2; //byteMsg[index++] = <DESTINO>
            crc16.addByte(byteMsg[index]);
            index = index + 1;

            ByteBuffer bbuffer = ByteBuffer.allocate(2);
            bbuffer.putShort((short) msgPart.length);
            byte[] payloadLengthByte = bbuffer.array();

            byteMsg[index] = payloadLengthByte[0];
            crc16.addByte(byteMsg[index]);
            index = index + 1;

            byteMsg[index] = payloadLengthByte[1];
            crc16.addByte(byteMsg[index]);
            index = index + 1;

            for (byte b : msgPart) {
                byteMsg[index] = b;
                crc16.addByte(byteMsg[index]);
                index = index + 1;
            }

            crc16.addByte(WhiteRedBlueProtocol.PAD_BYTE);

            byte[] crcBytes = crc16.CRC16();

            byteMsg[index] = crcBytes[0];
            index = index + 1;

            byteMsg[index] = crcBytes[1];
            index = index + 1;

            byteMsg[index] = WhiteRedBlueProtocol.PAD_BYTE;
            byteMsgList.add(byteMsg);
        }

        return byteMsgList;

    }

    @Override
    public void write(String msg, short payloadLength) {
        System.out.println(msg);
        for (char c : msg.toCharArray()) {
            System.out.printf("%X", (byte) c);
        }
        System.out.println("");
        List<byte[]> byteMsgList = this.parse(msg, payloadLength);

        int tries; //tentativas
        TESTE:
        for (byte[] frame : byteMsgList) {

            tries = 0;
            byte response = WhiteRedBlueProtocol.NACK_BYTE;

            do {
                try {
                    tries = tries + 1;
                    System.out.println("Tentativa: " + (tries));
                    response = connection.write(frame, true);
                    System.out.printf("RESPOSTA: %X\n", response);
                    Util.printByteArrayHexa(frame);
                } catch (TimeoutException ex) {
                    response = WhiteRedBlueProtocol.NACK_BYTE;
                    System.out.println("Timeout :(");
                    continue;
                }
            } while (response == WhiteRedBlueProtocol.NACK_BYTE && tries <= connection.getTriesNum());
        }
    }

    private static List<byte[]> split(byte[] byteMsg, int payloadLength) {
        int total = (byteMsg.length / payloadLength) + 1;
        List<byte[]> byteMsgList = new ArrayList<>();

        int from = 0;
        int to = 0;

        for (int i = 0; (i + 1) < total; i++) {
            from = i * payloadLength;
            to = ((i + 1) * payloadLength);
            byteMsgList.add(Arrays.copyOfRange(byteMsg, from, to));
        }

        if ((byteMsg.length % payloadLength) != 0) {
            from = (total - 1) * payloadLength;
            to = byteMsg.length;
            byteMsgList.add(Arrays.copyOfRange(byteMsg, from, to));
        }

        byteMsgList.add(new byte[0]); // Byte vazio para indicar final da mensagem.
        return byteMsgList;
    }

    @Override
    public void writeACKNACK(byte ACKNACK) {
        try {
            byte[] msg = new byte[3];
            msg[0] = WhiteRedBlueProtocol.SYN_BYTE;
            msg[1] = ACKNACK;
            msg[2] = WhiteRedBlueProtocol.PAD_BYTE;
            connection.write(msg, false);
            for (byte b : msg) {
                System.out.printf("%X", b);
            }
            System.out.println("");
        } catch (TimeoutException ex) {
            Logger.getLogger(WhiteRedBlueProtocolWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
