package br.unifor.redesI.projeto.protocol;

import br.unifor.redesI.projeto.connection.ConnectionListener;

/**
 *
 * @author Demétrio
 */
public interface ProtocolReader extends ConnectionListener<byte[]> {
}
