package br.unifor.redesI.projeto.protocol;

import java.util.List;

/**
 *
 * @author Demétrio
 */
public interface ProtocolWriter {

    List<byte[]> parse(String msg, short payloadLength);

    public void write(String msg, short payloadLength);
    
    public void writeACKNACK(byte ACKNACK);
}
