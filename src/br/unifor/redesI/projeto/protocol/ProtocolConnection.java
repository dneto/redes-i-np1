package br.unifor.redesI.projeto.protocol;

import br.unifor.redesI.projeto.connection.Connection;

/**
 *
 * @author Demétrio
 */
public interface ProtocolConnection extends Connection<String> {

    void sendACK();

    void sendNACK();
}
