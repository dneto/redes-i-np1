/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifor.redesI.projeto.main;

import br.unifor.redesI.projeto.util.AppModeEnum;
import br.unifor.redesI.projeto.util.DataBitsEnum;
import br.unifor.redesI.projeto.util.ParityEnum;
import br.unifor.redesI.projeto.util.StopBitsEnum;

/**
 *
 * @author Demétrio
 */
public class AppConfig {

    private int transmissonRate;
    private String serialPortName;
    private ParityEnum parity;
    private AppModeEnum appMode;
    private DataBitsEnum dataBits;
    private StopBitsEnum stopBits;

    public AppConfig(int transmissonRate, String serialPortName, ParityEnum parity, AppModeEnum appMode, DataBitsEnum dataBits, StopBitsEnum stopBits) {
        this.transmissonRate = transmissonRate;
        this.serialPortName = serialPortName;
        this.parity = parity;
        this.appMode = appMode;
        this.dataBits = dataBits;
        this.stopBits = stopBits;
    }

    public int getTransmissonRate() {
        return transmissonRate;
    }

    public void setTransmissonRate(int transmissonRate) {
        this.transmissonRate = transmissonRate;
    }

    public String getSerialPortName() {
        return serialPortName;
    }

    public void setSerialPortName(String serialPortName) {
        this.serialPortName = serialPortName;
    }

    public ParityEnum getParity() {
        return parity;
    }

    public void setParity(ParityEnum parity) {
        this.parity = parity;
    }

    public AppModeEnum getAppMode() {
        return appMode;
    }

    public void setAppMode(AppModeEnum appMode) {
        this.appMode = appMode;
    }

    public DataBitsEnum getDataBits() {
        return dataBits;
    }

    public void setDataBits(DataBitsEnum dataBits) {
        this.dataBits = dataBits;
    }

    public StopBitsEnum getStopBits() {
        return stopBits;
    }

    public void setStopBits(StopBitsEnum stopBits) {
        this.stopBits = stopBits;
    }
}
